package com.retail.core;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class Item {
	
	private static Item itemInstance = null;
	private Set<ItemPOJO> itemPOJOSet = new TreeSet<>(new Comparator<ItemPOJO>() {
        @Override
        public int compare(ItemPOJO o1, ItemPOJO o2) {
        	
            return Long.toString(o1.getUpc()).compareTo(Long.toString(o2.getUpc()));
        }
    });
	
	public static Item getItemInstance() {
		if (itemInstance == null) {
			itemInstance = new Item();
		}
		return itemInstance;
	}
	
	//Add all Item in the set
	public void addItemsData(long upc, String description, float price, float weight, String shippingMethod){
		
		ItemPOJO itemPOJO = new ItemPOJO();
		itemPOJO.setUpc(upc);
		itemPOJO.setDescription(description);
		itemPOJO.setPrice(price);
		itemPOJO.setWeight(weight);
		if(shippingMethod.equals("GROUND")) {
			itemPOJO.setShippingMethod(true);
		}
		else {
			itemPOJO.setShippingMethod(false);
		}
		itemPOJOSet.add(itemPOJO);
		
	}
	
	//Get all Items from the set
	public Set<ItemPOJO> getAllItems(){
		return itemPOJOSet;
		
	}
	
	//Calculate Ground Shipping cost
	public void calculateGroundShippingCost(ItemPOJO itemPOJO){
		
		float shippingcost = (float) (itemPOJO.getWeight()*2.5);
		shippingcost = (float) Math.ceil(shippingcost * 100) / 100;
		itemPOJO.setShippingCost(shippingcost);
		
	}
	
	//Calculate Air Shipping cost
	public void calculateAirShippingCost(ItemPOJO itemPOJO){
		int upcdigit =(int) (((itemPOJO.getUpc()/10))%10);
		float shippingcost = (float) (itemPOJO.getWeight()*upcdigit);
		shippingcost = (float) Math.ceil(shippingcost * 100) / 100;
		itemPOJO.setShippingCost(shippingcost);
		
	}

}
