package com.retail.core;

public class ItemPOJO {
	
	public long upc;
	public String description;
	public float price;
	public float weight;
	public boolean shippingMethod;
	public float shippingCost;
	public float getShippingCost() {
		return shippingCost;
	}
	public void setShippingCost(float shippingCost) {
		this.shippingCost = shippingCost;
	}
	public long getUpc() {
		return upc;
	}
	public void setUpc(long upc) {
		this.upc = upc;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	public boolean isShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(boolean shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

}
