package com.retail.core;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ItemMain {
	
	static Item item = Item.getItemInstance();
	
	public static void main(String args[]) {
		addData();
		calculateShipments();
		batchanddisplayReport();
	}
	
	//Add the static data
	public static void addData() {
		item.addItemsData(567321101987L, "CD � Pink Floyd, Dark Side Of The Moon", 19.99f, 0.58f, "AIR");
		item.addItemsData(567321101986L, "CD � Beatles, Abbey Road", 17.99f, 0.61f, "GROUND");
		item.addItemsData(567321101985L, "CD � Queen, A Night at the Opera", 20.49f, 0.55f, "AIR");
		item.addItemsData(567321101984L, "CD � Michael Jackson, Thriller", 23.88f, 0.50f, "GROUND");
		item.addItemsData(467321101899L, "iPhone - Waterproof Case", 9.75f, 0.73f, "AIR");
		item.addItemsData(477321101878L, "iPhone -  Headphones", 17.25f, 3.21f, "GROUND");
		//Additional Test Data
		/*item.addItemsData(667321101987L, "CD � Pink Floyd, Dark Side Of The Moon", 19.99f, 0.58f, "AIR");
		item.addItemsData(767321101986L, "CD � Beatles, Abbey Road", 17.99f, 0.61f, "GROUND");
		item.addItemsData(867321101985L, "CD � Queen, A Night at the Opera", 20.49f, 0.55f, "AIR");
		item.addItemsData(967321101984L, "CD � Michael Jackson, Thriller", 23.88f, 0.50f, "GROUND");
		item.addItemsData(887321101899L, "iPhone - Waterproof Case", 9.75f, 0.73f, "AIR");
		item.addItemsData(8977321101878L, "iPhone -  Headphones", 17.25f, 3.21f, "GROUND");
*/
	}
	
	//Here we will calculate Shipment costs
	public static void calculateShipments() {
	
		for (ItemPOJO itemPOJO : item.getAllItems()) {
			if(itemPOJO.isShippingMethod()) {
				item.calculateGroundShippingCost(itemPOJO);
			}
			else {
				item.calculateAirShippingCost(itemPOJO);
			}
		}
	}
	
	// Here we will batch and display the report
	public static void batchanddisplayReport() {
		LocalDateTime today=LocalDateTime.now();
		String shipmentMethod = "AIR";
		ItemPOJO itemPOJO = new ItemPOJO();
		List<ItemPOJO> itemList = new ArrayList<>();
		itemList.addAll(item.getAllItems());
		// Set batch size
		int batchSize = 6;
		//Set initial batch size
		int initialBatchLimit = 0;
		//Set end batch size
		int endBatchLimit = batchSize;
			for(int i=itemList.size();i>0;i--) {
				float totalShippingCost = 0;
				if(endBatchLimit<=itemList.size()) {
					System.out.format("\n\n%-100sDate:%s", "****Shipment Report****", today);
		            System.out.format("\n\n%-30s%-50s%-30s%-30s%-30s%-30s\n", "UPC", "Description", "Price", "Weight", "Ship Method", "Shipping Cost" );
		            System.out.println("\n");
					for(int j=initialBatchLimit; j<endBatchLimit;j++) {
						itemPOJO = itemList.get(j);
						//Here we will store Ground and Air as true and false, If it is true we will print Ground and if it is false we will print AIR
						if(itemPOJO.shippingMethod) {
							shipmentMethod = "GROUND";
						}
						else {
							shipmentMethod = "AIR";	
						}
	                    System.out.format("\n%-30s%-50s%-30s%-30s%-30s%-30s", itemPOJO.getUpc(),itemPOJO.getDescription(),itemPOJO.getPrice(),itemPOJO.getWeight(), shipmentMethod, itemPOJO.getShippingCost() );
	                    //Here we will calculate Shipping cost
						totalShippingCost += itemPOJO.getShippingCost();
					}
		              System.out.println("\n");
		              System.out.format("%-30s%145s", "TOTAL SHIPPING COST", totalShippingCost);
					initialBatchLimit = endBatchLimit;
					//endBatchLimit = endBatchLimit+batchSize;
					endBatchLimit +=batchSize;
				}
				else {
					break;
				}

			}
	}
	
	

}
